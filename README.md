# Movies search engine 🎞

## Description

This application is a movie search engine that utilizes the collection of data avaible in IMBD's database.

## Technologies

- ReactJs.
- Custom Hooks.
- UseContext.
- Fetch API data.
- Classless css framework.
- Scss modules.
